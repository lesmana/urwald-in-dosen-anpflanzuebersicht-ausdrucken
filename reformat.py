# urwald-in-dosen anpflanzübersicht ausdrucken
# https://gitlab.com/lesmana/urwald-in-dosen-anpflanzuebersicht-ausdrucken
# copyright lesmana zimmer lesmana@gmx.de
# lizensiert unter WTFPL http://www.wtfpl.net/about/

import sys

from bs4 import BeautifulSoup
from bs4.diagnose import diagnose

if len(sys.argv) != 2:
  print('program erwartet ein argument:')
  print('  die html datei mit der anpflanzübersicht tabelle')
  print('der latex code wird auf stdout ausgegeben')
  print('ein typischer aufruf sieht etwa so aus:')
  print('  python reformat.py daten.html > daten.tex')
  sys.exit(1)

filename = sys.argv[1]
htmlstring = open(filename).read()
soup = BeautifulSoup(htmlstring, 'html.parser')

pflanzen = []

for tr in soup.tbody.find_all('tr'):
  tds = tr.find_all('td')
  pflanze = {}
  attrs = ('name', 'gute', 'schlechte', 'keimer', 'abstand', 'standort')
  for idx, attr in enumerate(attrs):
    pflanze[attr] = tds[idx].get_text(' ', strip = True)
  pflanzen.append(pflanze)

for pflanze in pflanzen:
  print('\\begin{itemize}')
  print('\\begin{samepage}')
  print('\\item[] \\textbf{%s}' % pflanze['name'])
  for attr in ('keimer', 'abstand', 'standort'):
    print('\\item %s' % pflanze[attr])
  for attr in ('gute', 'schlechte'):
    print('\\item %s nachbarn: %s' % (attr, pflanze[attr] if pflanze[attr] else 'keine'))
  print('\\end{samepage}')
  print('\\end{itemize}')
