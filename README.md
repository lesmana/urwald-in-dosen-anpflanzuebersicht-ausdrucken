urwald-in-dosen anpflanzübersicht ausdrucken
--------------------------------------------

die tollen leute von urwald-in-dosen.de haben
eine tolle anpflanzübersicht in einer html tabelle zusammengestellt:

https://urwald-in-dosen.de/anpflanzuebersicht/

dieses program nimmt die daten aus der html tabelle
und packt es in ein pdf dokument so dass man es ausdrucken kann.

in demo.pdf kann man sehen wie die pdf am ende etwa aussehen wird.
demo.pdf wurde aus demo.html erstellt.
demo.html beinhaltet eine tabelle
im format wie im original
mit beispiel daten.

anleitung
---------

1. program runterladen

        git clone https://gitlab.com/lesmana/urwald-in-dosen-anpflanzuebersicht-ausdrucken.git

   oder vom browser https://gitlab.com/lesmana/urwald-in-dosen-anpflanzuebersicht-ausdrucken

2. die html runterladen und in program ordner speichern

        curl https://urwald-in-dosen.de/anpflanzuebersicht/ > original.html

3. make ausführen

        make

   demo.html wird als original.html verwendet
   falls noch keine original.html datei existiert.

4. fehlende werkzeuge und latex pakete installieren und noch mal make ausführen
   (wiederholen nach bedarf)

5. pdf ausdrucken

abhängigkeiten
--------------

 * beautifulsoup (python)
 * pdflatex
 * vermutlich noch weitere die ich vergessen habe

wissenswertes
-------------

die makefile ist so geschrieben das original.html niemals überschrieben wird.
wenn noch keine original.html existiert dann wird demo.html
zu original.html kopiert und verwendet.
wenn schon eine original.html existiert dann wird die nicht überschrieben.
d.h. änderungen an original.html muss der nutzer selber vornehmen.

ursprünglich hatte ich den curl befehl in der makefile.
das war cool bis ich mal offline gearbeitet habe
und ein `make -B` ausgeführt habe.
`make -B` erstellt alle targets neu.
so auch den curl befehl.
der hat dann meine original.html gelöscht
und ich konnte nicht mehr arbeiten.

hintergrund
-----------

ich hatte schwierigkeiten mit der html tabelle.
auf kleinen bildschirmen ist die tabelle schwer zu lesen
weil ich dauernd nach links und rechts scrollen muss.
beim ausdrucken bekomme ich nur ein bruchteil der tabelle auf einer seite.
oder, nach anpassungen am html code, brauche ich etwa 20 seiten.

meiner meinung nach ist eine tabelle
nicht die geeignete form für diese daten.
es gibt kaum grund die werte der spalten zu vergleichen.
und sortieren nach spalte werde ich wohl auch nie verwenden.

ich habe ein program geschrieben welches die daten
von der html tabelle rausholt und in einer latex liste packt.
die latex liste wird dann in pdf umgewandelt
und kann ausgedruckt werden.
die pdf hat nur noch 5 seiten.
in demo.pdf kann man sehen wie die pdf am ende etwa aussehen wird.

aus urheberrechtlichen gründen
werde ich die pdf mit den vollständigen daten nicht hochladen.
urheberrecht der daten liegen
bei den tollen leuten von urwald-in-dosen.de.
mein urheberrecht beschränkt sich wohl lediglich auf das program.
ob die umformung selbst uhreberrechtswürdig ist weis ich nicht.

ich stelle das program frei zur verfügung so dass du für dich selbst
auch eine pdf erstellen und ausdrucken kannst.

lizenz
------

die daten von urwald-in-dosen anpflanzübersicht
sind urheberrecht von den leuten bei urwald-in-dosen.de.

dieses program (python code, makefile, latex code)
ist urheberrecht lesmana zimmer.

dieses program ist freie software.
es ist lizenziert unter der WTFPL.
das bedeutet ganz grob dass du machen kannst was du willst damit.
genaue details kannst du hier nachlesen: http://www.wtfpl.net/about/
