# urwald-in-dosen anpflanzübersicht ausdrucken
# https://gitlab.com/lesmana/urwald-in-dosen-anpflanzuebersicht-ausdrucken
# copyright lesmana zimmer lesmana@gmx.de
# lizensiert unter WTFPL http://www.wtfpl.net/about/

anpflanzübersicht.pdf: anpflanzübersicht.tex daten.tex Makefile
	pdflatex -halt-on-error -interaction nonstopmode anpflanzübersicht.tex

daten.tex: original.html reformat.py
	python reformat.py original.html > daten.tex

# download original.html yourself using
#     curl https://urwald-in-dosen.de/anpflanzuebersicht/ > original.html
# this rule exists so can run make -B (force redo everything)
# it will copy demo.html only if original.html not already exists
# bonus: the entire process can be tested using demo.html
original.html:
	cp -n demo.html original.html
